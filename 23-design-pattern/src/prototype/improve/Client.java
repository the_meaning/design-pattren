package prototype.improve;

/**
 * 原型模式：其实就是创建重复对象的同时，保证性能。
 *          该模式就是实现了一个接口(办法)，该接口(办法)用于创建当前对象的克隆
 *
 *    浅拷贝
 *    这里使用 Cloneable 的clone 办法来克隆对象，但是这只是浅拷贝，拷贝对象里的引用对象的时候，是直接把地址拷贝过来，
 *    这就导致所有拷贝对象里的引用对象都是指向同一个对象
 */

public class Client {

    public static void main(String[] args) {
        System.out.println("原型模式完成对象的创建");
        // TODO Auto-generated method stub
        Sheep sheep = new Sheep("tom", 1, "白色");

        sheep.friend = new Sheep("jack", 2, "黑色");

        Sheep sheep2 = (Sheep) sheep.clone(); //克隆
        Sheep sheep3 = (Sheep) sheep.clone(); //克隆
        Sheep sheep4 = (Sheep) sheep.clone(); //克隆
        Sheep sheep5 = (Sheep) sheep.clone(); //克隆

        System.out.println("sheep2 =" + sheep2 + "sheep2.friend=" + sheep2.friend.hashCode());
        System.out.println("sheep3 =" + sheep3 + "sheep3.friend=" + sheep3.friend.hashCode());
        System.out.println("sheep4 =" + sheep4 + "sheep4.friend=" + sheep4.friend.hashCode());
        System.out.println("sheep5 =" + sheep5 + "sheep5.friend=" + sheep5.friend.hashCode());

        System.out.println("sheep2 = sheep3 ? :" + (sheep2 == sheep3));
        System.out.println("sheep2.hashcode = " + sheep2.hashCode());
        System.out.println("sheep3.hashcode = " + sheep3.hashCode());
    }

}