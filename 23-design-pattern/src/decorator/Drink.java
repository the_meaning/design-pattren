package decorator;

// 装饰者模式的抽象类
public abstract class Drink {

    private String desc;

    private float price;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    // 这杯饮料花费的金额
    abstract float cost();
}
