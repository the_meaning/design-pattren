package decorator;

// 意大利咖啡
public class Espresso extends Coffee {

    public Espresso() {
        setDesc("意大利咖啡");
        setPrice(10f);
    }
}
