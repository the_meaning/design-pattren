package decorator;

// 缓冲层 可要可不要
// 共性多的话可以提个父类出来，这里做个例子
public class Coffee extends Drink{
    @Override
    float cost() {
        return super.getPrice();
    }
}
