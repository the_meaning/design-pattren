package decorator;

// 调料装饰者
// 饮料 + 调料 实际上还是饮料
public class Decorator extends Drink{

    // 要装饰的饮料
    private Drink drink;

    public Decorator(Drink drink) {
        this.drink = drink;
    }

    // 调料的花费 因为这里是调料装饰在饮料上返回饮料
    // 所以这里的价格 = 饮料 + 调料
    @Override
    public float cost() {
        return drink.cost() + getPrice();
    }

    // 输出当前组合
    @Override
    public String getDesc() {
        return super.getDesc() + getPrice()  + " && " + drink.getDesc();
    }
}
