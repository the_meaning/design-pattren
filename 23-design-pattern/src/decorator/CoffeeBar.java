package decorator;

/**
 * 装饰者模式：动态的将新功能附加到对象上，在对象扩展方面，比继承更有弹性
 */

// 咖啡店
public class CoffeeBar {

    public static void main(String[] args) {
        // 做一杯意大利咖啡 + 牛奶 + 巧克力
        Drink drink = new Espresso(); // 意大利咖啡
        System.out.println("咖啡描述：" + drink.getDesc());
        System.out.println("咖啡价格：" + drink.cost());

        drink = new Milk(drink); // 加上牛奶
        System.out.println("咖啡+牛奶描述：" + drink.getDesc());
        System.out.println("咖啡+牛奶价格：" + drink.cost());

        drink = new Chocolate(drink); // 加上巧克力
        System.out.println("咖啡+牛奶+巧克力描述：" + drink.getDesc());
        System.out.println("咖啡+牛奶+巧克力价格：" + drink.cost());
    }
}
