package decorator;
// 澳洲黑咖啡
public class LongBlack extends Coffee{

    public LongBlack() {
        setDesc("澳洲黑咖啡");
        setPrice(12f);
    }
}
