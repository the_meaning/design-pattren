package command;

/**
 * 抽象出来的命令接口
 *
 */
public interface Command {
    // 执行接口
    void execute();

    // 撤销接口
    void undo();
}
