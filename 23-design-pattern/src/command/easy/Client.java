package command.easy;

/**
 * 该模式通过“命令”这种对象在办法的请求者和实现者之间传递实现解耦。
 * 比如餐厅，客人来了通过菜单点菜，餐厅再煮菜。餐厅提供的菜单相当于把请求和处理进行解耦。
 *
 * 简易版的命令模式
 */
public class Client {

    public static void main(String[] args) {
        Command cmd = new ConcreteCommand(); // 命令的执行者聚合在命令之中
        Invoker ir = new Invoker(cmd); // 命令的请求者（调用者）
        System.out.println("客户访问调用者的call()方法...");
        ir.call();
    }
}
