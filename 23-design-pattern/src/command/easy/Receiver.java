package command.easy;

// 接收者
// 命令的执行者
public class Receiver {

    public void action() {
        System.out.println("接收者的action()方法被调用...");
    }
}
