package command.easy;

/**
 * 简易版命令模式
 */

// 命令接口
public interface Command {
    public abstract void execute();
}
