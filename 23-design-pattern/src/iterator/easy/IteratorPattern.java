package iterator.easy;

/**
 * 迭代器模式：
 *      提供一种遍历集合元素的统一接口，用一致的办法遍历集合元素，不需要知道集合对象的底层结构，即：不暴露内部结构。
 *
 * 简易原理版本
 */
public class IteratorPattern {

    public static void main(String[] args) {
        Aggregate ag = new ConcreteAggregate();
        ag.add("中山大学");
        ag.add("华南理工");
        ag.add("韶关学院");
        System.out.print("聚合的内容有：");
        Iterator it = ag.getIterator();
        while (it.hasNext()) {
            Object ob = it.next();
            System.out.print(ob.toString() + "\t");
        }
        Object ob = it.first();
        System.out.println("\nFirst：" + ob.toString());
    }
}
