package bridge;

public class Client {

    public static void main(String[] args) {
        Vivo vivo = new Vivo();
        FoldedPhone foldedVivoPhone = new FoldedPhone(vivo);
        foldedVivoPhone.open();
        foldedVivoPhone.call();
        foldedVivoPhone.close();

        System.out.println();

        UpRightPhone upRightVivoPhone = new UpRightPhone(vivo);
        upRightVivoPhone.open();
        upRightVivoPhone.call();
        upRightVivoPhone.close();
    }
}
