package bridge;

/**
 * 桥接模式
 * 将实现和抽象分离开来（可以这么理解，有多个维度需要实现（形状、颜色），单靠实现继承来写耦合性高，也容易类爆炸），
 * 使得两个层次可以独立改变。（类似桥一样，可拆可搭，耦合性低，其实就是少继承，多关联）
 */

// 品牌
public interface Brand {

    void open();// 开机

    void call();

    void close();
}
