package visitor.easy;

/**
 * 访问者模式：
 * 某种数据结构中有各种元素，把对这些元素的操作分离出来封装成独立的类。为每个元素提供多种访问方式。
 * <p>
 * 简易原理版本
 */
public class VisitorPattern {
    public static void main(String[] args) {
        ObjectStructure os = new ObjectStructure();
        os.add(new ConcreteElementA());
        os.add(new ConcreteElementB());
        Visitor visitor = new ConcreteVisitorA();
        os.accept(visitor);
        System.out.println("------------------------");
        visitor = new ConcreteVisitorB();
        os.accept(visitor);
    }
}
