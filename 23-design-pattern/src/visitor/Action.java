package visitor;

// 这里的 Action 相当与访问者。
// Person 相当与元素
public abstract class Action {

    //得到男性 的测评
    public abstract void getManResult(Man man);

    //得到女的 测评
    public abstract void getWomanResult(Woman woman);
}
