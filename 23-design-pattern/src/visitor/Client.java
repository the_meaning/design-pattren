package visitor;

/**
 * 访问者模式
 *      某种数据结构中有各种元素，把对这些元素的操作分离出来封装成独立的类。为每个元素提供多种访问方式。
 * 举具体例子：歌手评分
 */
public class Client {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        //创建ObjectStructure
        ObjectStructure objectStructure = new ObjectStructure();

        objectStructure.attach(new Man());
        objectStructure.attach(new Woman());


        //成功
        Success success = new Success();
        objectStructure.display(success);

        System.out.println("===============");
        Fail fail = new Fail();
        objectStructure.display(fail);

        System.out.println("=======给的是待定的测评========");

        Wait wait = new Wait();
        objectStructure.display(wait);
    }
}
