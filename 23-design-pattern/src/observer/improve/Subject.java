package observer.improve;

// 相当于一个发布信息的网站，比如天气预报订阅站什么的
public interface Subject {

    void registerObserver(Observer o);
    void removeObserver(Observer o);
    void notifyObservers();
}
