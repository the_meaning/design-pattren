package observer.improve;

// 观察者接口，之所以叫观察者，因为一有变化就要体现出来
public interface Observer {
    void update(float temperature, float pressure, float humidity);
}
