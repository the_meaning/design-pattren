package observer;

/**
 * 普通方式实现天气预报通知
 * 缺点：如果又有第三方网站来使用 WeatherData天气网站 的数据，那 WeatherData 中又要新增相应的对象，改代码，不合理
 *
 * 然后在 improve 包中使用--观察者模式来改进
 */
public class Client {
    public static void main(String[] args) {
        //创建接入方 currentConditions
        CurrentConditions currentConditions = new CurrentConditions();
        //创建 WeatherData 并将 接入方 currentConditions 传递到 WeatherData中
        WeatherData weatherData = new WeatherData(currentConditions);

        //更新天气情况
        weatherData.setData(30, 150, 40);

        //天气情况变化
        System.out.println("============天气情况变化=============");
        weatherData.setData(40, 160, 20);
    }
}

