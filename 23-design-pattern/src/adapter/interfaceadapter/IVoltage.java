package adapter.interfaceadapter;

public interface IVoltage {

    void voltage180v();

    void voltage220v();

    void voltage240v();

    void voltage290v();
}
