package adapter.interfaceadapter;

/**
 * 接口适配器模式：
 *      当不需要全部实现一个接口的办法时，可先设计一个抽象类实现接口，并先使用空实现所有办法，
 *      然后该抽象类的子类就可以有选择的覆盖某个/某些办法来实现具体的需求
 */

public class AdapterClient {

    public static void main(String[] args) {

        // 这里匿名内部类 本质：都写了一个{} 类定义的括号了 ，相当与写了一个子类继承了父类，括号里就是子类的定义，只不过这个子类只使用一次，没定义类名。
        AbstractAdapter adapter = new AbstractAdapter() {
            @Override
            public void voltage220v() {
                System.out.println("将 220v 转换为 5v");
            }
        };

        adapter.voltage220v();
    }
}
