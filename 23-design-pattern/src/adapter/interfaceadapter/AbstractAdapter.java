package adapter.interfaceadapter;

public abstract class AbstractAdapter implements IVoltage {

    @Override
    public void voltage180v() {

    }

    @Override
    public void voltage220v() {

    }

    @Override
    public void voltage240v() {

    }

    @Override
    public void voltage290v() {

    }
}
