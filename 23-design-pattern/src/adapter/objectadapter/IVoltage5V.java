package adapter.objectadapter;

/**
 * 适配的接口 dest
 * 这里是要适配成 5V 的电压给手机充电
 */

public interface IVoltage5V {
    int output5V();
}
