package adapter.objectadapter;

public class Client {

    public static void main(String[] args) {
        System.out.println("对象适配器模式===");
        Phone phone = new Phone();
        Voltage220V inputVoltage = new Voltage220V();
        VoltageAdapter adapter = new VoltageAdapter(inputVoltage);
        phone.charge(adapter);
    }
}
