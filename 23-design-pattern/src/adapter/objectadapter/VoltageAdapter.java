package adapter.objectadapter;

/**
 * 对象适配器模式：
 *      6·
 *
 */
public class VoltageAdapter implements IVoltage5V {

    private Voltage220V voltage220V;

    public VoltageAdapter(Voltage220V inputVoltage) {
        this.voltage220V = inputVoltage;
    }

    // 适配过程，将 220V 转换成 5V 电压
    @Override
    public int output5V() {
        int srcElectric = voltage220V.output220V();
        int destElectric = srcElectric / 44;
        return destElectric;
    }
}
