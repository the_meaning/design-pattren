package adapter.classadapter;

/**
 * 手机类：
 * 充电
 */
public class Phone {
    public void charge(IVoltage5V iVoltage5V) {
        if (iVoltage5V.output5V() == 5) {
            System.out.println("电压为 5V，可以给手机充电");
        } else {
            System.out.println("电压不符合充电标准，不能给手机充电");
        }
    }
}
