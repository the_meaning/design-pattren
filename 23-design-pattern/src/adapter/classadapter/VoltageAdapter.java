package adapter.classadapter;

/**
 * 适配类
 * 具体职责：适配，即将220V 转换成 5V
 *
 * 类适配器模式：
 *      这里通过继承 Voltage220V 来适配成 5V 电压输出
 *      缺点：要继承一个类，耦合性高，不符合之前学习的
 *                              合成服用原则：少继承，多组合
 *
 */
public class VoltageAdapter extends Voltage220V implements IVoltage5V{
    // 适配过程，将 220V 转换成 5V 电压
    @Override
    public int output5V() {
        int srcElectric = output220V();
        int destElectric = srcElectric / 44;
        return destElectric;
    }
}
