package adapter.classadapter;

/**
 * 源 src ，被适配的源
 * 这里是电压 220V ，需要被适配给手机充电
 */
public class Voltage220V {

    //输出220V的电压
    public int output220V() {
        int src = 220;
        System.out.println("电压=" + src + "伏");
        return src;
    }

}
