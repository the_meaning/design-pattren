package singleton.lazytype.type2;

/**
 * 懒汉式
 */
public class LazySingle {
    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getInstance();
        Singleton singleton2 = Singleton.getInstance();
        System.out.println(singleton1 == singleton2);
    }
}

// 第二种写法：线程安全
class Singleton {
    private Singleton(){}

    private static Singleton instance;
    // 只有一个线程会进入 synchronized，但是有缺点，每个线程都会被卡在这里，效率太低了，
    // 但其实这个实例化办法只需要执行一次就可以了
    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
