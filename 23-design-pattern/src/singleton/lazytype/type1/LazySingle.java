package singleton.lazytype.type1;

/**
 * 懒汉式
 */
public class LazySingle {
    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getInstance();
        Singleton singleton2 = Singleton.getInstance();
        System.out.println(singleton1 == singleton2);
    }
}

// 第一种写法：线程不安全
class Singleton {
    private Singleton(){}

    private static Singleton instance;

    public static Singleton getInstance() {
        if (instance == null) {
            // 可能有多个线程同时进入到这一步
            instance = new Singleton();
        }
        return instance;
    }
}
