package singleton.lazytype.type3;

/**
 * 懒汉式
 */
public class LazySingle {
    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getInstance();
        Singleton singleton2 = Singleton.getInstance();
        System.out.println(singleton1 == singleton2);
    }
}

// 第三种写法：双重检测锁（DCL）
// 注意要使用 volatile 来避免指令重排
// 因为双重检测不一定线程安全，因为有指令重排的存在
class Singleton {
    private Singleton(){}

    private static volatile Singleton instance;
    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
