package singleton.innertype;

/**
 * 静态内部类的方式
 */
public class InnerClassType {
    public static void main(String[] args) {
//        System.out.println("使用静态内部类完成单例模式");
//        Singleton instance = Singleton.getInstance();
//        Singleton instance2 = Singleton.getInstance();
//        System.out.println(instance == instance2); // true
//        System.out.println("instance.hashCode=" + instance.hashCode());
//        System.out.println("instance2.hashCode=" + instance2.hashCode());
        Singleton.say();
    }
}

// 静态内部类完成， 推荐使用
// （外部类的加载不会导致内部类的加载）
// Singleton 类加载时，不会去加载 SingletonInstance，当调用 SingletonInstance.INSTANCE 时，
// 才回去加载内部类 SingletonInstance，从而保证了懒加载
class Singleton {
    private static volatile Singleton instance;

    //构造器私有化
    private Singleton() {}

    //写一个静态内部类,该类中有一个静态属性 Singleton
    private static class SingletonInstance {
        static {
            System.out.println("静态内部类加载了");
        }
        private static final Singleton INSTANCE = new Singleton();
    }

    //提供一个静态的公有方法，直接返回SingletonInstance.INSTANCE
    public static synchronized Singleton getInstance() {
        return SingletonInstance.INSTANCE;
    }

    public static void say() {
        System.out.println("hello");
    }
}