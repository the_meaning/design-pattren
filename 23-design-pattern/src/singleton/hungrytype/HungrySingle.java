package singleton.hungrytype;

/**
 * 饿汉式
 */


public class HungrySingle {
    public static void main(String[] args) {

    }

}

// 第一种写法：静态常量 写法
class Singleton1 {
    private static final Singleton1 instance = new Singleton1();

    private Singleton1(){};

    public static Singleton1 getSingleton() {
        return instance;
    }
}

// 第二种写法：静态代码块 写法
class Singleton2 {
    private Singleton2(){};

    private static Singleton2 instance;

    static {
        instance = new Singleton2();
    }

    public static Singleton2 getSingleton() {
        return instance;
    }
}
