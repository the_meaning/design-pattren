package flyweight;
/**
 * 享元模式： 也叫蝇量模式
 * 		注重点在共享对象（享元），它通过共享已经存在的对象来大幅度减少需要创建的对象数量、避免大量相似类的开销，从而提高系统资源的利用率。
 *
 * 	典型应用如：线程池、String 常量池
 *
 * 	这里有两个概念：
 * 		外部状态：无法共享的的结构，这里指新闻使用的用户 User
 * 		内部状态：可以共享的结构，不必重复创建，这里指新闻类型 type
 *
 */
public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// 创建一个工厂类
		WebSiteFactory factory = new WebSiteFactory();

		// 客户要一个以新闻形式发布的网站
		WebSite webSite1 = factory.getWebSiteCategory("新闻");


		webSite1.use(new User("tom"));

		// 客户要一个以博客形式发布的网站
		WebSite webSite2 = factory.getWebSiteCategory("博客");

		webSite2.use(new User("jack"));

		// 客户要一个以博客形式发布的网站
		WebSite webSite3 = factory.getWebSiteCategory("博客");

		webSite3.use(new User("smith"));

		// 客户要一个以博客形式发布的网站
		WebSite webSite4 = factory.getWebSiteCategory("博客");

		webSite4.use(new User("king"));

		System.out.println("网站的分类共 = " + factory.getWebSiteCount());
	}

}