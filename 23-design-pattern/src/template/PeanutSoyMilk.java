package template;

// 花生豆浆
// 具体子类
public class PeanutSoyMilk extends SoyMilk {
    @Override
    void addCondiments() {
        System.out.println("第二步，加上好的花生");
    }
}
