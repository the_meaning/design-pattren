package template;

/**
 * 模板办法模式：
 *      定义一个操作中的算法骨架，而将算法的一些步骤延迟到子类中，使得子类可以不改变该算法结构的情况下重定义该算法的某些特定步骤。
 *
 *      这里的 make() 办法就是骨架，里面可以搭建各种办法的使用架构，办法可供子类具体实现
 */
public abstract class SoyMilk {

    // 一般模板办法都会加一个 final 防止子类重写
    final void make() {
        select();
        addCondiments();
        soak();
        beat();
    }

    //选材料
    void select() {
        System.out.println("第一步：选择好的新鲜黄豆  ");
    }

    //添加不同的配料， 抽象方法, 子类具体实现
    abstract void addCondiments();

    //浸泡
    void soak() {
        System.out.println("第三步， 黄豆和配料开始浸泡， 需要3小时 ");
    }

    void beat() {
        System.out.println("第四步：黄豆和配料放到豆浆机去打碎  ");
    }

}
