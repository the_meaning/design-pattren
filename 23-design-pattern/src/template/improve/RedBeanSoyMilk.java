package template.improve;


// 红豆豆浆
// 具体子类
public class RedBeanSoyMilk extends SoyMilk {
    @Override
    void addCondiments() {
        System.out.println("第二步，加上好的红豆");
    }
}
