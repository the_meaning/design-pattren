package template.improve;

/**
 * 这里加入钩子办法 customerWantCondiments 可以有默认实现/给子类具体实现，以此来控制一些流程
 *
 * 钩子办法：定义一个办法，默认不做任何事情，子类可以视情况覆盖，该办法称为钩子
 */
public abstract class SoyMilk {

    // 一般模板办法都会加一个 final 防止子类重写
    final void make() {
        select();
        if (customerWantCondiments()) {
            addCondiments();
        }
        soak();
        beat();
    }

    //选材料
    void select() {
        System.out.println("第一步：选择好的新鲜黄豆  ");
    }

    //添加不同的配料， 抽象方法, 子类具体实现
    abstract void addCondiments();

    //浸泡
    void soak() {
        System.out.println("第三步， 黄豆和配料开始浸泡， 需要3小时 ");
    }

    void beat() {
        System.out.println("第四步：黄豆和配料放到豆浆机去打碎  ");
    }

    //钩子方法，决定是否需要添加配料
    boolean customerWantCondiments() {
        return true;
    }

}
