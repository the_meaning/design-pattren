package factory.simple.pizzastore.order;

// 测试工厂生产 pizza
// （可以当成客户端点披萨）
public class TestMain {
    public static void main(String[] args) {
        //使用简单工厂模式
        new OrderPizza(new SimpleFactory());
        System.out.println("~~退出程序~~");
    }
}
