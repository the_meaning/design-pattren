package builder.daily;

/**
 * 普通写法
 * 建造一个房子：
 * 步骤包括：打地基、砌墙、盖房顶
 *
 * 缺点：把产品（房子）和创建产品的过程封装在了一起，耦合性太强
 */

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CommonHouse commonHouse = new CommonHouse();
		commonHouse.build();
	}

}
