package builder.improve;

public class CommonBuilding extends HouseBuilder{

    @Override
    void buildBasic() {
        house.setBaise("5m");
        System.out.println("普通房子打地基 5 米");
    }

    @Override
    void buildWall() {
        house.setWall("10cm");
        System.out.println("普通房子砌墙 10 厘米");
    }

    @Override
    void buildRoof() {
        house.setRoofed("钢筋屋顶");
        System.out.println("普通房子盖屋顶");
    }
}
