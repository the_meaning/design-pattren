package builder.improve;

public class HignBuilding extends HouseBuilder{

    @Override
    void buildBasic() {
        house.setBaise("50m");
        System.out.println("高楼大厦打地基 50 米");
    }

    @Override
    void buildWall() {
        house.setWall("20cm");
        System.out.println("高楼大厦砌墙 20 厘米");
    }

    @Override
    void buildRoof() {
        house.setRoofed("玻璃屋顶");
        System.out.println("高楼大厦盖屋顶");
    }
}
