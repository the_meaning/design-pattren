package builder.improve;

/**
 * 建造者
 * 可以是接口、也可以是抽象类
 *
 */

public abstract class HouseBuilder {

    protected House house = new House();

    abstract void buildBasic();

    abstract void buildWall();

    abstract void buildRoof();

    // 将建造好的产品返回
    public House getHouse() {
        return house;
    }
}
