package builder.improve;

/**
 * 建造者模式：
 * 可以将复杂对象的建造过程抽象出来，这个抽象的过程又可以又不同的实现，可以构造出不同的对象。（注重构建的过程）
 *
 * 将构造过程抽离出来其实就降低耦合性了
 *
 * 一般会有四种角色：
 *  1 产品
 *  2 定义产品各个部件生产办法的（抽象类/接口）
 *  3 具体的构建者（实现上面的抽象类/接口）
 *  4 指挥者（决定构建的过程（比如顺序什么的））
 */

public class Client {
    public static void main(String[] args) {
        //盖普通房子
        CommonBuilding commonHouse = new CommonBuilding();
        //准备创建房子的指挥者
        HouseDirector houseDirector = new HouseDirector(commonHouse);

        //完成盖房子，返回产品(普通房子)
        House house = houseDirector.buildingHouse();

        //System.out.println("输出流程");

        System.out.println("--------------------------");
        //盖高楼
        HignBuilding highBuilding = new HignBuilding();
        //重置建造者
        houseDirector.setHouseBuilder(highBuilding);
        //完成盖房子，返回产品(高楼)
        houseDirector.buildingHouse();
    }
}
