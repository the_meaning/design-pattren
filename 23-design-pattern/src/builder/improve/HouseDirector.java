package builder.improve;

public class HouseDirector {

    // 屋子构造者
    private HouseBuilder houseBuilder;

    // 方式一 通过构造办法传入
    public HouseDirector(HouseBuilder builder) {
        this.houseBuilder = builder;
    }

    // 方式二 通过 setter 办法传入
    public void setHouseBuilder(HouseBuilder builder) {
        this.houseBuilder = builder;
    }

    // 构造房子的流程，可以自定义先砌墙还是先打地基（打比方）
    public House buildingHouse() {
        houseBuilder.buildBasic();
        houseBuilder.buildWall();
        houseBuilder.buildRoof();
        return houseBuilder.getHouse();
    }

}
