package proxy.staticproxy;

public interface ITeacherDao {
    void tech();
}
