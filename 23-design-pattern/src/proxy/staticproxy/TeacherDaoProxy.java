package proxy.staticproxy;

public class TeacherDaoProxy implements ITeacherDao{

    public ITeacherDao target;

    public TeacherDaoProxy(ITeacherDao target) {
        this.target = target;
    }

    @Override
    public void tech() {
        System.out.println("执行前=== 记录日志");
        target.tech();
        System.out.println("执行后=== 记录日志");
    }
}
