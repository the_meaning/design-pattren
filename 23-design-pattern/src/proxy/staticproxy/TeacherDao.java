package proxy.staticproxy;

public class TeacherDao implements ITeacherDao{
    @Override
    public void tech() {
        System.out.println("教师授课中。。。");
    }
}
