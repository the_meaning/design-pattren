package proxy.cglibproxy;

/**
 * cglib 动态代理，也称 子类代理，好处就是不用像jdk动态代理那样实现一个接口
 */

public class TeacherDao {
    public void tech() {
        System.out.println("cglib 动态代理-教师授课中。。。");
    }
}
