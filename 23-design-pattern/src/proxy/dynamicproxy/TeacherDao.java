package proxy.dynamicproxy;

import proxy.dynamicproxy.ITeacherDao;

public class TeacherDao implements ITeacherDao {
    @Override
    public void tech() {
        System.out.println("动态代理-教师授课中。。。");
    }
}
