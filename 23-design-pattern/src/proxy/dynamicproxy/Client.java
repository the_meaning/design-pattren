package proxy.dynamicproxy;

public class Client {

    public static void main(String[] args) {
        // 要代理的目标
        ITeacherDao target = new TeacherDao();

        ProxyFactory proxyFactory = new ProxyFactory(target);
        ITeacherDao proxyInstance = (ITeacherDao) proxyFactory.getProxyInstance();
        proxyInstance.tech();
    }

}
