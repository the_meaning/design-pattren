package proxy.dynamicproxy;

public interface ITeacherDao {
    void tech();
}
