package inversion.improve;

/**
 * 依赖关系传递的三种方式：
 * 1，接口传递
 * 2，构造办法传递
 * 3，setter方式传递
 */
public class ThreeWay {



}

// 方式一：通过接口传递实现依赖
interface IOpenAndClose {
    void open(ITV itv);
}

interface ITV {
    void play();
}

class OpenAndClose implements IOpenAndClose { // 实现类：实现一个 tv 的开机运行
    @Override
    public void open(ITV itv) {
        itv.play();
    }
}

// 方式二：通过构造办法传递依赖
interface IOpenAndClose2 {
    void open();
}

interface ITV2 {
    void play();
}

class OpenAndClose2 implements IOpenAndClose2 { // 实现类：实现一个 tv 的开机运行
    private ITV2 itv2;

    public OpenAndClose2 (ITV2 itv2) {
        this.itv2 = itv2;
    }

    @Override
    public void open() {
        itv2.play();
    }
}

// 方式三： 通过 setter 办法传递
interface IOpenAndClose3 {
    void open();
}

interface ITV3 {
    void play();
}

class OpenAndClose3 implements IOpenAndClose3 { // 实现类：实现一个 tv 的开机运行
    private ITV3 itv3;

    public void setItv3(ITV3 itv3) {
        this.itv3 = itv3;
    }

    @Override
    public void open() {
        itv3.play();
    }
}
