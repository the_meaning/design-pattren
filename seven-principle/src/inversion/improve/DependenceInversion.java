package inversion.improve;

/**
 * 优化
 */

public class DependenceInversion {
    public static void main(String[] args) {
        Person person = new Person();
        person.receive(new Email());
        person.receive(new Weixin());
    }
}

interface IReceive {
    String getInfo();
}

class Email implements IReceive{
    @Override
    public String getInfo() {
        return "email info: hello world!";
    }
}

class Weixin implements IReceive{
    @Override
    public String getInfo() {
        return "weixin info: hello world!";
    }
}

class Person {
    public void receive(IReceive receive) {
        System.out.println(receive.getInfo());
    }
}
