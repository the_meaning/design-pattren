package inversion;

/**
 * 依赖倒转原则：
 * 高层模块不应该依赖低层模块，即要面向接口编程，不要面向实现编程（即高层使用接口或抽象类定制好规范，底层使用实现类实现）
 */

public class DependenceInversion {
    public static void main(String[] args) {
        /**
         * 分析问题：如果现在不止接收email ，也要接收微信了，那这里的可扩展性就不行
         * 优化：引入一个接口 IReceive 接口，Email 和 Weixin 各自实现该接口，我们就依赖接口，不依赖具体类，这样就实现了依赖倒置原则
         */
        Person person = new Person();
        person.receive(new Email());
    }
}

class Email {
    String getInfo() {
        return "email info: hello world!";
    }
}

class Person {
    public void receive(Email email) {
        System.out.println(email.getInfo());
    }
}
