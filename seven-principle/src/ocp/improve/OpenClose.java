package ocp.improve;

/**
 * 使用开闭原则优化，新增一个三角形时看看之前的区别
 */

public class OpenClose {
    public static void main(String[] args) {

        GraphicEditor graphicEditor = new GraphicEditor();
        graphicEditor.drawShape(new Rectangle());
        graphicEditor.drawShape(new Circle());
        graphicEditor.drawShape(new Triangle());
    }
}


class GraphicEditor { // 新增三角形时，不用修改使用测（实现类）的代码
    public void drawShape(Shape shape) {
        shape.draw();
    }

}

// 形状 基类    使用抽象类/接口
abstract class Shape {
    void draw(){};
}

class Rectangle extends Shape {
    @Override
    void draw() {
        System.out.println("绘制矩形");
    }
}

class Circle extends Shape {
    @Override
    void draw() {
        System.out.println("绘制圆形");
    }
}

class Triangle extends Shape { // 新增三角形
    @Override
    void draw() {
        System.out.println("绘制三角形");
    }
}


