package ocp;

/**
 * 开闭原则：所有原则的基础，对扩展开放，对修改关闭。
 */

public class OpenClose {
    public static void main(String[] args) {
        /**
         * 这里如果要新增一个图形，就需要改很多：
         */
        GraphicEditor graphicEditor = new GraphicEditor();
        graphicEditor.drawShape(1);
        graphicEditor.drawShape(2);
        graphicEditor.drawShape(3);
    }
}


class GraphicEditor {
    public void drawShape(int type) {
        if (type == 1) {
            drawRectangle();
        }
        if (type == 2) {
            drawCircle();
        }
        if (type == 3) { // 新增三角形 ，还要修改使用侧代码
            drawTriangle();
        }
    }

    public static void drawRectangle() {
        System.out.println("绘制矩形");
    }

    public static void drawCircle() {
        System.out.println("绘制圆形");
    }

    public static void drawTriangle() { // 新增三角形 ，还要修改使用侧代码
        System.out.println("绘制三角形");
    }

}

// 形状 基类
class Shape {
    int type;
}

class Rectangle extends Shape {
    public Rectangle() {
        super.type = 1; // 矩形
    }
}

class Circle extends Shape {
    public Circle() {
        super.type = 2; // 圆形
    }
}

class Triangle extends Shape { // 新增三角形
    public Triangle() {
        super.type = 3;
    }
}



