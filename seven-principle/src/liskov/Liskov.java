package liskov;

/**
 * 里氏替换原则：子类可以扩展父类的功能，但不能改变父类原有的功能
 *          子类尽量不要重写父类的办法，能做到透明使用子类，即使用子类替换所有父类使用到的地方，功能不变，不会报错
 */

public class Liskov {
    public static void main(String[] args) {
        // 字面意思理解，不举例了
    }
}
