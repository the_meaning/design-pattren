package segregation.improve;

/**
 * 改进
 */

public class Segregation {
    public static void main(String[] args) {
        C c = new C();
        c.depend1(new A());
        c.depend2(new A());
        c.depend3(new A());

        D d = new D();
        d.depend1(new B());
        d.depend4(new B());
        d.depend5(new B());
    }
}

// 接口一有五个办法，类A 和 类B 继承了，类C 和 类D 通过接口依赖A、B 来使用接口中的一部分办法
interface Interface1 {
    void method1();
}
interface Interface2 {
    void method2();
    void method3();
}
interface Interface3 {
    void method4();
    void method5();
}

class A implements Interface1, Interface2 {

    @Override
    public void method1() {
        System.out.println("A implements method1");
    }

    @Override
    public void method2() {
        System.out.println("A implements method2");
    }

    @Override
    public void method3() {
        System.out.println("A implements method3");
    }
}

class B implements Interface1, Interface3 {

    @Override
    public void method1() {
        System.out.println("B implements method1");
    }

    @Override
    public void method4() {
        System.out.println("B implements method4");
    }

    @Override
    public void method5() {
        System.out.println("B implements method5");
    }
}

class C {
    public void depend1(Interface1 i) {
        i.method1();
    }

    public void depend2(Interface2 i) {
        i.method2();
    }

    public void depend3(Interface2 i) {
        i.method3();
    }
}

class D {
    public void depend1(Interface1 i) {
        i.method1();
    }

    public void depend4(Interface3 i) {
        i.method4();
    }

    public void depend5(Interface3 i) {
        i.method5();
    }
}

