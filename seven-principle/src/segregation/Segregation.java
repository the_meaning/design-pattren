package segregation;

/**
 * 接口隔离原则：
 * 接口尽量小，一个接口只服务于一个子模块或业务逻辑（客户端不应该依赖他不需要的接口[ 比如接口中有多个办法，只需要一部分办法的话，应该把接口拆分出来 ]）
 */

public class Segregation {
    public static void main(String[] args) {
        // 类C 通过接口来依赖类A ，但是只会使用其中三个办法，这样明显不符合接口隔离原则
        C c = new C();
        c.depend1(new A());
        c.depend2(new A());
        c.depend3(new A());

        D d = new D();
        d.depend1(new B());
        d.depend4(new B());
        d.depend5(new B());
    }
}

// 接口一有五个办法，类A 和 类B 继承了，类C 和 类D 通过接口依赖A、B 来使用接口中的一部分办法
interface Interface1 {
    void method1();

    void method2();

    void method3();

    void method4();

    void method5();
}

class A implements Interface1 {

    @Override
    public void method1() {
        System.out.println("A implements method1");
    }

    @Override
    public void method2() {
        System.out.println("A implements method2");
    }

    @Override
    public void method3() {
        System.out.println("A implements method3");
    }

    @Override
    public void method4() {
        System.out.println("A implements method4");
    }

    @Override
    public void method5() {
        System.out.println("A implements method5");
    }
}

class B implements Interface1 {

    @Override
    public void method1() {
        System.out.println("B implements method1");
    }

    @Override
    public void method2() {
        System.out.println("B implements method2");
    }

    @Override
    public void method3() {
        System.out.println("B implements method3");
    }

    @Override
    public void method4() {
        System.out.println("B implements method4");
    }

    @Override
    public void method5() {
        System.out.println("B implements method5");
    }
}

class C {
    public void depend1(Interface1 i) {
        i.method1();
    }

    public void depend2(Interface1 i) {
        i.method2();
    }

    public void depend3(Interface1 i) {
        i.method3();
    }
}

class D {
    public void depend1(Interface1 i) {
        i.method1();
    }

    public void depend4(Interface1 i) {
        i.method4();
    }

    public void depend5(Interface1 i) {
        i.method5();
    }
}

